-- INTRODUCTION --
DC radiant is a simple and elegant Drupal 7 responsive theme 
designed for cross browser/cross device websites and is suitable for corporate site. 
The responsive slideshow is created by using CSS.
A flexible and a light weight theme.
  Simple and clean design
  Drupal standards compliant

-- REQUIREMENTS --
This theme is compatible with Drupal 7.x.x
No base theme needed, simply use this theme on its own.

-- BROWSER COMPATIBILITY --
The theme has been tested on following browsers: 
Firefox.

-- INSTALLATION --
Unpack and upload the entirety of this directory in sites/all/themes/ directory.
Navigate to admin/appearance. Make it active, set default DC radiant.
Click the 'Save Configuration' button at the bottom.

-- CONFIGURATION --
Configure theme settings in Administration � Appearance � Settings � DC radiant
Click the 'Save Configuration' button at the bottom.

-- CREATED A RESPONSIVE FRAME --
First, we need to make the outer slider element responsive.
<div id="slider">
<figure>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide12.jpg" alt>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide11.jpg" alt>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide10.jpg" alt>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide9.jpg" alt>
<img src="<?php print $base_path . $directory; ?>/images/slideshows/slide8.jpg" alt>
</figure> <!-- end of slider -->
</div>
(I�m leaving the alt attribute blank to save on space; it would be filled out appropriately 
for SEO purposes).
Note that the css for the slideshow is placed for 5 images which is in page.tpl.php, 
allowing the animation we will build to repeat in a smooth loop.

The window is given a width of 95% to make it responsive, and a max-width that corresponds 
to the natural width of an individual image (1100px, in the case of the example), 
as we don�t want to make any image larger than it naturally is:
div#slider {
width: 95%;
max-width: 1100px;
margin: auto; }

In our CSS, the width of the figure is described as a percentage multiplier of the div that 
contains it. That is, if imagestrip contains five images, and the final div shows just one, 
the figure is 5x wider, or 500% the width of the container div:
div#slider figure {
position: relative;
width: 500%;
margin: 0;
padding: 0;
font-size: 0;
text-align: left; }

We have to evenly divide the photographs inside the imagestrip.
100% / 5 = 20%
Which leads to the following CSS declaration:
div#slider figure img{
width: 20%;
height: auto;
float: left; }

Finally, we hide the overflow of the div:
div#slider {
width: 95%;
max-width: 1100px;
overflow: hidden; }

The imagestrip moving from left to right. If the containing div is 100% wide, then each 
movement of the imagestrip to the left will be measured in increments of that distance:
@keyframes slidy { 
0% { left: 0%; }
20% { left: 0%; }
25% { left: -100%; }
45% { left: -100%; }
50% { left: -200%; }
70% { left: -200%; } 
75% { left: -300%; }
95% { left: -300%; }
100% { left: -400%; }
}

We have to call on the animation to get things started.
div#slider figure {
position: relative;
width: 500%;
margin: 0;
padding: 0;
font-size: 0;
left: 0;
text-align: left;
animation: 30s slidy infinite; }
