<div id="page-wrapper"><div id="page">
   <header>
    <div class="wrapp">
      <div class="logo">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="dc-site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>
      </div>

      <?php if ($site_name || $site_slogan): ?>
        <?php if ($site_name): ?>
          <h1 class="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
          <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      <?php endif; ?>

      <?php print render($page['header']); ?>

      <div class="clear"></div>

      <?php if ($main_menu == TRUE): ?>
        <nav id="main-menu"  role="navigation">
      <!-- <a class="nav-toggle" href="#"><?php print t("Navigation"); ?></a>-->
            <div class="menu-navigation-container">
              <?php if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
              ?>
            </div>
            <div class="clear"></div>
        </nav>
       <?php endif; ?><!-- /#main-menu -->

    </div>
   </header><!-- /.header -->

<div id="slider">
<figure>
    <img src="<?php print $base_path . $directory; ?>/images/slideshows/slide12.jpg" alt>
    <img src="<?php print $base_path . $directory; ?>/images/slideshows/slide11.jpg" alt>
    <img src="<?php print $base_path . $directory; ?>/images/slideshows/slide10.jpg" alt>
    <img src="<?php print $base_path . $directory; ?>/images/slideshows/slide9.jpg" alt>
    <img src="<?php print $base_path . $directory; ?>/images/slideshows/slide8.jpg" alt>
</figure> <!-- end of slider -->
</div>
   
    <div class="dc_pure-content wrapp">

      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_first']); ?>
        </div></div> <!-- /.section, /#sidebar-first -->
      <?php endif; ?>

		<div id="content" class="column"><div class="section">
		<?php if (theme_get_setting('breadcrumbs')): ?>
          <div id="breadcrumbs">
            <?php if ($breadcrumb): print $breadcrumb;
            endif;
            ?></div>
        <?php endif; ?>
		
        <div class="wrapp section">
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          <?php print render($tabs); ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
          <section id="whatnew">
            <div class="wrapp accordion"><?php print render($page['accordion']); ?></div>
          </section><!-- /#whatnew -->
          <section id="featured" class="grey">
            <div class="wrapp feature"><?php print render($page['featured']); ?></div>
          </section><!-- /#featured -->
        </div>
		  
        </div></div> <!-- /.section, /#content -->

      <?php if ($page['sidebar_second']): ?>
		<div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
        </div></div> <!-- /.section, /#sidebar-second -->
      <?php endif; ?>
	  
    </div><!-- /.dc_pure-content, /.wrapp -->


      <?php if ($page['footer']): ?>
    <div class="footer" class="clearfix">
      <?php print render($page['footer']); ?>
    </div><!-- /.footer -->
      <?php endif; ?>            

  </div></div> <!-- /#page, /#page-wrapper -->